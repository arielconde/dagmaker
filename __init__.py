""""
DAGMaker
"""

import os
import subprocess
import yaml

from airflow.plugins_manager import AirflowPlugin
from flask import Blueprint, request, redirect, url_for
from flask_admin import BaseView, expose

from dagmaker import dagmaker
from dagmaker.dagmaker import dag_creation, validations, helpers


# FIXME: Use SSH authentication on Git Instead
# PROJECT_SOURCE_TYPES = ['DS Shared', 'Git Repository']
PROJECT_SOURCE_TYPES = ['DS Shared']



def create_new_dag(project_source,
                   project_source_type,
                   git_username='',
                   git_password=''):
    """
    Create a new dag base on project source type
    If project source type is 'Git Repository'
        1. clone to project to DAGMAKER_PROJECTS_DIR using load_project_from_git
        2. then validate if there is no project with the same dag name

    If project source type is 'DS Shared'
        1. Validate if there is no project with the same dag name
        2. Then copy the project to a folder name as the dag name

    Then build the DAGs
    """
    if project_source_type == 'Git Repository':
        if git_username != '' and git_password != '':
            project_directory = dagmaker.load_project_from_git(project_source,
                                                               git_username,
                                                               git_password)

            # Validate that the dagname is unique before building the ag
            if validations.is_dag_name_take(project_source):
                raise Exception("DAG name defined on prject is already taken")
        else:
            return "Git username and password are not provided"

    elif project_source_type == 'DS Shared':
        # Validate that the dag name is unique before copying
        if validations.is_dag_name_taken(project_source):
            raise Exception("DAG name defined on prject is already taken")
        project_directory = dagmaker.load_project_from_directory(project_source)

    else:
        raise Exception(f"Unknown project source type received: {project_source_type}")

    dagmaker.build_dag_from_project(project_directory)
    return "DAG created"


def update_dag(
    project_source,
    project_source_type,
    git_username='',
    git_password=''
):
    if project_source_type == 'Git Repository':
        if git_username != '' and git_password != '':
            project_directory = dagmaker.update_project_from_git(project_source,
                                                                 git_username,
                                                                 git_password)
            dagmaker.build_dag_from_project(project_directory)
        else:
            return "Git username and password are not provided"
    elif project_source_type == 'DS Shared':
        project_directory = dagmaker.update_project_from_directory(project_source)
        dagmaker.build_dag_from_project(project_directory)
    else:
        return f"Unknown project source type received: {project_source_type}"


def delete_dag(project_name):
    dagmaker.delete_project(project_name)


class DagMaker(BaseView):
    """
    Defines the views of DAGMaker
    """
    @expose('/')
    def index(self):
        return self.render("dagmaker/index.html",
                           dagmaker_projects=helpers.get_dagmaker_projects(),
                           project_source_types=PROJECT_SOURCE_TYPES)

    @expose("/create")
    def create_new_dag(self):
        project_source = request.args.get('project_source', type=str)
        project_source_type = request.args.get('project_source_type', type=str)
        git_username = request.args.get('git_username', default='', type=str)
        git_password = request.args.get('git_password', default='', type=str)

        create_new_dag(project_source,
                       project_source_type,
                       git_username,
                       git_password)

        return redirect(url_for('dagmaker.index'))

    @expose("/update")
    def update_dag(self):
        project_source = request.args.get('project_source', type=str)
        project_source_type = request.args.get('project_source_type', type=str)
        git_username = request.args.get('git_username', default='', type=str)
        git_password = request.args.get('git_password', default='', type=str)

        update_dag(project_source, project_source_type,
                   git_username, git_password)

        return redirect(url_for('dagmaker.index'))

    @expose('/delete')
    def delete_dag(self):
        project_name = request.args.get('project_name', type=str)
        delete_dag(project_name)
        return redirect(url_for('dagmaker.index'))


admin_view_ = DagMaker(category="Tools", name="Dag Maker")
blue_print_ = Blueprint("dag_maker",
                        __name__,
                        template_folder='dagmaker_templates',
                        static_folder='static',
                        static_url_path='/static/dagmaker')


class AirflowDagMakerPlugin(AirflowPlugin):
    name             = "dag_maker"
    admin_views      = [admin_view_]
    flask_blueprints = [blue_print_]
