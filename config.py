import os
from dagmaker.dagmaker.tasks import task_r, task_dummy, task_python, task_jupyter


AIRFLOW_HOME = os.getenv('AIRFLOW_HOME', '/opt/airflow_home')

# Diretory where to store copies of projects using DAGMaker
DAGMAKER_PROJECTS_DIR = f"{AIRFLOW_HOME}/projects/dagmaker"

# Directory where to store DAGs of dagmaker projects
DAGMAKER_DAGS_DIR = f"{AIRFLOW_HOME}/dags/dagmaker"

# DAG template to use
DAG_TEMPLATE = f"{AIRFLOW_HOME}/plugins/dagmaker/dagmaker/dag_templates/dag_with_email.txt"

# Maximum project size allowed - in MB
MAX_PROJECT_SIZE = 1000

tasks_types_operators_mapping_functions = {
    'R': task_r,
    'dummy': task_dummy,
    'Python': task_python,
    'Jupyter': task_jupyter,
}
