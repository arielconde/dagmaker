import os
from dagmaker.dagmaker import helpers

def is_dagmaker_project(project_directory):
    if 'DAGMaker.yml' in os.listdir(project_directory):
        return True
    
def is_valid_dagmaker_yml(dagmaker_yml_path):
    # TODO: Validate a dagmaker.yml file
    return True


def is_dag_name_taken(project_directory):
    """
    Checks if a dagname is already taken given
    the project directory
    """
    dagname = helpers.get_dag_name(project_directory)
    dagmaker_projects = helpers.get_dagmaker_projects()

    # Find the dagname in the projects
    for dagmaker_project in dagmaker_projects:
        if dagmaker_project['name'] == dagname:
            return True
    return False
