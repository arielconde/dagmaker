"""
This is contains the functions used to create the DAG files
"""
import datetime
import yaml

from dagmaker import dagmaker, config
from dagmaker.dagmaker import helpers


def create_task(task, project_directory):
    """
    This calls the create_tasks function of
    tasks_types_operators_mapping_functions defined in the config
    The create_tasks function should return a string of the definiton
    of the task
    """
    if task['type'] in config.tasks_types_operators_mapping_functions.keys():
        return config.tasks_types_operators_mapping_functions[task['type']]\
               .create_task(task, project_directory)
    raise Exception("Unknown task type:", task['type'])


def create_dag(project_directory):
    """
    Create the dafil
    """
    dagmaker_file = f"{project_directory}/DAGMaker.yml"
    with open(dagmaker_file) as f:
        dag_config = yaml.full_load(f)

    dag_name = dag_config['name'].replace(" ", "_").replace(" ", ".")
    owner = dag_config['owner']
    notify_emails = dag_config['notify_emails']
    schedule = dag_config['schedule']
    tasks_order = dag_config['tasks_order']

    now_year = datetime.datetime.now().year
    now_month = datetime.datetime.now().month
    now_day = datetime.datetime.now().day

    schedule = 'None' if schedule in ['None', None] else f"'{schedule}'"
    dag_file = helpers.get_dag_file(project_directory)

    tasks_template = ""
    for task in dag_config['tasks']:
        if tasks_template == "":
            tasks_template = create_task(task, project_directory) + "\n"
        else:
            tasks_template = tasks_template + "    " + create_task(task, project_directory) + "\n"

    tasks_order_template = ""
    if len(dag_config['tasks']) > 1:
        for task_order in tasks_order:
            tasks_order_template = tasks_order_template + task_order + "\n"

    with open(config.DAG_TEMPLATE) as file:
        template = file.read()

    template = template.format(
        dag_name=dag_name,
        schedule=schedule,
        owner=owner,
        now_year=now_year,
        now_month=now_month,
        now_day=now_day,
        tasks_definition=tasks_template,
        tasks_order=tasks_order_template,
        creation_time=datetime.datetime.now(),
        notify_emails=notify_emails
    )

    print("Writing to:", dag_file)

    with open(dag_file, 'w+') as file:
        file.write(template)
