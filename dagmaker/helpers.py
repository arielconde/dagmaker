"""
helper functions
"""

import os
import datetime
import subprocess

import yaml

from dagmaker import config


DAGMAKER_PROJECTS_DIR = config.DAGMAKER_PROJECTS_DIR


def write_to_dagmaker_info_file(project_dir,
                                project_source,
                                project_source_type):
    """
    Creates DAGMaker.info file in on creation dagmaker projects
    DAGMaker.info file stores metadata about the project
    :param project_dir: The project directory in this airflow instance
    :param project_source: The source of this project, either a Git URL or
        a filepath in the server this airflow instance is running
    """
    with open(f"{project_dir}/DAGMaker.info", 'w+') as file:
        file.write(f"Source: {project_source}\n")
        file.write(f"Source Type: {project_source_type}\n")
        file.write(f"Last Updated: {datetime.datetime.now()}")


def get_git_url_with_username(repository_url, username):
    """
    Returns the GitURL with username
    Example:
        repository_url: https://gitlab.com/user/sample-project
        username: user
        will return https://user@gitlab.com/user/sample-project

    :param repository_url: Git repository URL
    :param username: Username to add to the Git URL
    """
    protocol = repository_url.split("://")[0]
    path = repository_url.split("://")[1]
    git_url_with_username = f"{protocol}://{username}@{path}"
    return git_url_with_username


def get_git_domain(repository_url, with_protocol=False):
    """
    Returns the git domain, example - https://gitlab.com/user/sample-project
    will return gitlab.com or (with protocol) https://gitlab.com
    :param repository_url: Git repository URL
    :param with_protocol: To include the http or https
    """
    protocol = repository_url.split("://")[0]
    domain = repository_url.split("://")[1].split("/")[0]
    if with_protocol:
        return f"{protocol}://{domain}"
    return domain


def get_project_directory_from_repository(repository_url):
    """
    Returns the location where the project will be copied in this
    airflow instance
    :param repository_url: Git repository URL, must be ending in ".git"
    """
    project_name = repository_url.split("/")[-1].replace(".git", "")
    project_directory = f"{DAGMAKER_PROJECTS_DIR}/{project_name}"
    return project_directory


def get_dag_file(project_directory):
    """
    Returns the filepath of DAG file for the project,
    by reading the DAGMaker.yml file
    """
    with open(f'{project_directory}/DAGMaker.yml') as file:
        dag_config = dag_config = yaml.full_load(file)
    dag_name = dag_config['name'].replace(" ", "_").replace(" ", ".")
    return f"{config.DAGMAKER_DAGS_DIR}/{dag_name}.py"


def get_dag_name(project_directory):
    """
    Returns the dagname by reading the DAGMaker.yml
    """
    with open(f'{project_directory}/DAGMaker.yml') as file:
        dag_config = dag_config = yaml.full_load(file)
    dag_name = dag_config['name'].replace(" ", "_").replace(" ", ".")
    return dag_name


def get_project_source(project_directory):
    """
    Returns the project source (Git URL or DS Shared Folder path) by reading
    the DAGMaker.info file (see create_dagmaker_info_file function)
    """
    try:
        dagmaker_info_file = f"{project_directory}/DAGMaker.info"
        with open(dagmaker_info_file) as file:
            source = [line for line in file.readlines()
                      if 'Source:' in line][0]
            source = source.split('Source:')[1].strip()
            return source
    except Exception as e:
        return f"Unable to get project source of {project_directory} {e}"


def get_project_source_type(project_directory):
    """
    Returns the project source type (Git URL or DS Shared Folder path)
    by reading the DAGMaker.info file (see create_dagmaker_info_file function)
    """
    try:
        dagmaker_info_file = f"{project_directory}/DAGMaker.info"
        with open(dagmaker_info_file) as file:
            source = [line for line in file.readlines()
                      if 'Source Type:' in line][0]
            source = source.split('Source Type:')[1].strip()
            return source
    except Exception as e:
        return f"Unable to get project source type of {project_directory} {e}"


def get_last_updated(project_directory):
    """
    Returns the last update timestamp of this project
    by reading the DAGMaker.info file (see create_dagmaker_info_file function)
    """
    try:
        dagmaker_info_file = f"{project_directory}/DAGMaker.info"
        with open(dagmaker_info_file) as file:
            source = [line for line in file.readlines()
                      if 'Last Updated:' in line][0]
            source = source.split('Last Updated:')[1].strip()
            return source
    except Exception as e:
        return f"Unable to get last updated of {project_directory} {e}"


def get_dagmaker_projects():
    """
    Returns a list of the metadata of the projects stored DAGMAKER_PROJECTS_DIR
    """
    dagmaker_projects_dir_contents = os.listdir(DAGMAKER_PROJECTS_DIR)
    dagmaker_projects = []

    for project in dagmaker_projects_dir_contents:
        project_dir = f"{DAGMAKER_PROJECTS_DIR}/{project}"
        if os.path.isdir(project_dir):
            project_contents = os.listdir(project_dir)
            if 'DAGMaker.yml' in project_contents:
                with open(f"{project_dir}/DAGMaker.yml") as file:
                    dag_config = yaml.full_load(file)

                dagmaker_project = {
                    'name': get_dag_name(project_dir),
                    'source': get_project_source(project_dir),
                    'source_type': get_project_source_type(project_dir),
                    'project_dir': project_dir,
                    'owner': dag_config['owner'],
                    'schedule': dag_config['schedule'],
                    'last_updated': get_last_updated(project_dir)
                }
                dagmaker_projects.append(dagmaker_project)
    return dagmaker_projects


def get_directory_size(path):
    """
    Returns the size of path in MB
    :param path: :)
    """
    size = subprocess.check_output(['du', '-shm', path])
    size = size.split()[0].decode('utf-8')
    return int(size)
