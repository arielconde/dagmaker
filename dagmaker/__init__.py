import os
import subprocess
import configparser
import shutil
import datetime

import yaml

import pexpect

from dagmaker import config
from dagmaker.dagmaker import validations, helpers, dag_creation


DAGMAKER_PROJECTS_DIR = config.DAGMAKER_PROJECTS_DIR
MAX_PROJECT_SIZE = config.MAX_PROJECT_SIZE


def load_project_from_git(repository_url, username, password):
    """
    Clones the project from the repository url, this uses pexpect to
    perform authentication like using using a terminal
    :param repository_url: Git URL
    :param username: Git Useranme
    :param username: Git Password

    :return project_directory: The directory where this project is copied
    """
    project_directory = helpers.get_project_directory_from_repository(repository_url)

    # Raise an error if the project already exists
    if os.path.exists(project_directory):
        raise Exception(f"{project_directory} already exists, to update \
            existing project use update_project_from_git(repository_url, \
            username, password)")

    # Perform the git clone to project_directory
    git_clone_command = f'git clone {repository_url} {project_directory}'
    child = pexpect.spawn(git_clone_command)

    # Handle a prompt asking for username
    git_domain = helpers.get_git_domain(repository_url, with_protocol=True)
    expected_username_prompt = f"Username for '{git_domain}'"
    child.expect(expected_username_prompt)
    child.sendline(username)

    # Handle the prompt asking for password
    git_url_with_username = helpers.get_git_url_with_username(repository_url, username)
    expected_password_prompt = f"Password for '{git_url_with_username}': "
    child.expect(expected_password_prompt)
    child.sendline(password)

    # Get the output of the git clone command
    output = child.read().decode()
    child.close()

    # Raise an Exception if an error is encountered while cloning the project
    if child.exitstatus != 0:
        raise Exception(f"Unable to clone project from repository {output}")

    # TODO: Check if a valid dagmaker project
    if not validations.is_dagmaker_project(project_directory):
        shutil.rmtree(project_directory)
        raise Exception(f"Project is not a valid dagmaker project. \
                        Deleting {project_directory}")

    # Log the metadata in in DAGMaker.info file inside the copied project
    helpers.write_to_dagmaker_info_file(project_directory,
                                        repository_url, 'Git Repository')

    return project_directory


def update_project_from_git(repository_url, username, password):
    """
    Clones the project from the repository url, this uses pexpect to
    perform authentication like using using a terminal
    :param repository_url: Git URL
    :param username: Git Useranme
    :param username: Git Password
    :return project_directory: The directory where this project is copied
    """
    project_directory = helpers.get_project_directory_from_repository(repository_url)

    if not os.path.exists(project_directory):
        raise Exception(f"{project_directory} does not exists, \
            did you mean to create a new project instead?")

    # Perfom git pull, by changing workind directory to proejct_directory
    child = pexpect.spawn(f'git pull {repository_url}', cwd=project_directory)

    # Handle prompt asking for username
    git_domain = helpers.get_git_domain(repository_url, with_protocol=True)
    expected_username_prompt = f"Username for '{git_domain}'"
    child.expect(expected_username_prompt)
    child.sendline(username)

    # Handle prompt asking for password
    git_url_with_username = helpers.get_git_url_with_username(repository_url, username)
    expected_password_prompt = f"Password for '{git_url_with_username}': "
    child.expect(expected_password_prompt)
    child.sendline(password)

    # Get the output of the git clone command
    output = child.read().decode()
    child.close()

    if child.exitstatus != 0:
        raise Exception(f"Unable to pull project from repository {output}")

    # Log the metadata in in DAGMaker.info file inside the copied project
    helpers.write_to_dagmaker_info_file(project_directory,
                                        repository_url, 'Git Repository')

    return project_directory


def load_project_from_directory(source_project_directory):
    """
    Copies a project from the source_project_directory to DAGMAKER_PROJECTS_DIR
    The name of the directory of the copied project is same as dagname
    :param source_project_directory: The directory of the source project in
        this airflow instance
    """

    # Validate if a it's dagmaker project
    if 'DAGMaker.yml' not in os.listdir(source_project_directory):
        raise Exception(f"DAGMaker.yml not found in {source_project_directory}")

    # Validate that project size must not exceed MAX_PROJECT_SIZE
    project_size = helpers.get_directory_size(source_project_directory)
    if project_size > MAX_PROJECT_SIZE:
        raise Exception(f"Project ({project_size}) exceeded MAX_PROJECT_SIZE: \
            {MAX_PROJECT_SIZE}")

    # project_name will be the name of the project directory
    # under DAGMAKER_PROJECTS_DIR
    project_name = helpers.get_dag_name(source_project_directory)

    source_project_directory = source_project_directory.replace(" ", "\\ ")
    destination_project_directory = f"{DAGMAKER_PROJECTS_DIR}/{project_name}".replace(" ", "\\ ")

    # Create the destination directory
    if not os.path.exists(destination_project_directory):
        os.makedirs(destination_project_directory)

    # Perform rsync to copy files
    command = f"rsync -avu {source_project_directory}/* {destination_project_directory}"
    print("Executing:", command)
    proc = subprocess.run(f"({command})", stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE, shell=True)

    if proc.returncode != 0:
        raise Exception(f"{proc.stdout.decode()} {proc.stderr.decode()}")
    else:
        print(f"{proc.stdout.decode()} {proc.stderr.decode()}")
        print(f"{source_project_directory} is copied to {destination_project_directory}")

    # Write to DAGMaker.info to track project
    helpers.write_to_dagmaker_info_file(destination_project_directory,
                                source_project_directory, 'DS Shared')

    return destination_project_directory


def update_project_from_directory(source_project_directory):
    """
    Just call load_project_from_directy since we're using rsync to copy files
    rsync just picks up the recent changes from the source directory
    :param source_project_directory: directory to pick updates from
    """

    # Validate if a it's dagmaker project
    if 'DAGMaker.yml' not in os.listdir(source_project_directory):
        raise Exception(f"DAGMaker.yml not found in {source_project_directory}")

    project_directory = load_project_from_directory(source_project_directory)
    return project_directory


def delete_project(project_name):
    """
    Deletes the project directory in DAGMAKER_PROJECTS_DIR
    and the dag file in DAGMAKER_DAGS_DIR
    :param project_name: The name of the project to delete
    """

    # Get all the dagmaker projects
    dagmaker_projects = helpers.get_dagmaker_projects()

    # Search for project_name in dagmaker_projects
    project_to_delete = None
    for dagmaker_project in dagmaker_projects:
        if project_name == dagmaker_project['name']:
            project_to_delete = dagmaker_project

    if project_to_delete is not None:
        # Delete the dag file
        dag_file_to_delete = helpers.get_dag_file(project_to_delete['project_dir'])
        print(f"Deleting dag file {dag_file_to_delete}")
        os.remove(dag_file_to_delete)

        # Delete the project folder
        print(f"Deleting {project_to_delete['project_dir']}")
        shutil.rmtree(project_to_delete['project_dir'])
        return True
    return False


def build_dag_from_project(project_directory):
    """
    Build the DAG given the project directory

    This function must perform some validation tests on the tasks
    defined on the dag

    :param project_directory: The project directory
    """
    if not validations.is_dagmaker_project(project_directory):
        raise Exception(f"{project_directory} is not a valid DAGMaker project")

    dagmaker_file = f"{project_directory}/DAGMaker.yml"

    print("Validating DAGMaker.yml file")
    if not validations.is_valid_dagmaker_yml(dagmaker_file):
        raise Exception("There is some errors in your DAGMaker.yml file")

    # TODO: Validate each tasks
    #

    print("Creating the DAGFile \n\n")
    dag_creation.create_dag(project_directory)
