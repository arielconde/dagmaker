"""
Create Airflow Tasks for Python Scripts
"""
import yaml
import os
import datetime
import subprocess
import datetime


# Required
def create_task(task, project_directory):
    # Get the absolute path of the script from the project_directory
    script_location = f"{project_directory}/{task['script_loc']}"
    
    bash_command = f"python '{script_location}'"
    return f"{task['name']} = BashOperator(task_id='{task['name']}', bash_command=\"{bash_command}\")"