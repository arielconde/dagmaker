"""
Define what operator doesk task_r will use
This are the funtions used in R tasks in DAGMaker
"""
import yaml
import os
import datetime
import subprocess
import datetime


def create_task(task, project_directory):
    if task['version'] == 'default':
        rscript = f"Rscript"
    else:
        rscript = get_rscript_executable(task['version'])

    script_location = f"{project_directory}/{task['script_loc']}"

    # Perform some validations here
    # validate_r_script(script_location)

    # Perform some setup here
    # setup_r_script(script_location, rscript_executable)

    bash_command = f"{rscript} '{script_location}'"
    return f"{task['name']} = BashOperator(task_id='{task['name']}', bash_command=\"{bash_command}\")"


def lintr_script(script_loc, rscript='Rscript'):
    proc = subprocess.run(
        [rscript, '-e', 'lintr::lint(commandArgs(trailingOnly = TRUE))', script_loc],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    if proc.returncode == 0:
        lintr_styles = []
        lintr_errors = []
        lintr_warnings = []
        lintr_others = []
        stdout = proc.stdout.decode().strip()
        if stdout != '':
            full_script_loc = stdout.split(":")[0]
            lintr_results = stdout.split(full_script_loc)
            lintr_results.remove('')
            for lintr_result in lintr_results:
                if lintr_result.split()[1] == 'style:':
                    lintr_styles.append(lintr_result)
                elif lintr_result.split()[1] == 'error:':
                    lintr_errors.append(lintr_result)
                elif lintr_result.split()[1] == 'warning:':
                    lintr_warnings.append(lintr_result)
                else:
                    lintr_others.append(lintr_result)   
        results = {
            'witherror': len(lintr_errors) > 0,
            'lintr_styles': lintr_styles,
            'lintr_errors': lintr_errors,
            'lintr_warnings': lintr_warnings,
            'lintr_others': lintr_others
        }
        if results['witherror'] == True:
            print(f"Found {len(results['lintr_errors'])} error/s on {script_loc}: {results['lintr_errors']}")   
        return results
    else:
        raise Exception(f"Unable to lint R script {script_loc}", proc.stderr.decode())
        
        
def all_required_packages_installed(script_loc, rscript='Rscript', optional_packages=[]):
    required_packages = get_required_packages(script_loc)
    uninstalled_packages = []
    for package in required_packages:
        if package_exists(package, rscript) == False:
            uninstalled_packages.append(package)
    if len(uninstalled_packages) > 0:
        print(f"The following packages are not installed:", uninstalled_packages)
        return False
    else:
        return True
    
def get_required_packages(script_loc):
    import re
    packages = []
    with open(script_loc) as f:
        lines = f.readlines()
        for line in lines:
            if line.strip()[:1] == '#': # Ignore commented lines
                continue
            package = re.search(r'library\((.*)\)', line)
            if package != None:
                package = package.group(1)
                package = package.replace('"', '').replace("'", "")
                packages.append(package)
    return packages


def package_exists(package, rscript='Rscript'):
    import subprocess
    proc = subprocess.run(
        [rscript, '-e', f'library({package})'],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    return proc.returncode == 0
    
    
def install_packages(packages, rscript='Rscript'):
    import subprocess
    packages_status = {'packages': {}, 'errors': 0, 'packages_with_error': []}
    for package in packages:
        if package_exists(package, rscript) == False:
            proc = subprocess.run(
                [rscript, "-e", f"install.packages('{package}', repos='https://cran.stat.upd.edu.ph')"],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )

            packages_status['packages'][package] = {
                'returncode': proc.returncode,
                'stdout': proc.stdout.decode(),
                'stderr': proc.stderr.decode(),
                'witherror': proc.stderr.decode() != ''
            }
        else:
            packages_status['packages'][package] = {
                'returncode': 0,
                'stdout': '',
                'stderr': '',
                'witherror': False
            }
            
    for package in packages_status['packages'].keys():        
        if packages_status['packages'][package]['witherror'] == True:
            packages_status['errors'] += 1
            packages_status['packages_with_error'].append(package)
            print(f"Unable to install package {package}:", packages_status['packages'][package]['stderr'])
    return packages_status


def validate_r_script(rscript_location):
    if os.path.isfile(rscript_location) == False:
        raise Exception(f"File not found {rscript_location}")
        
    lintr_results = lintr_script(rscript_location)
    if lintr_results['witherror']:
        raise Exception(f"Your R script {script_location} has errors {lintr_results['lintr_errors']}")
        
    windows_shared_access = find_windows_shared_access(rscript_location)
    if len(windows_shared_access) != 0:
        print(f"Warning your script is referencing something in windows shared format '//10.51.2.73' please use the server format instead /data: {windows_shared_access}")
        
        
def setup_r_script(rscript_location, rscript):
    # Ensure that all required packages are present
    required_packages = get_required_packages(rscript_location)
    install_status = install_packages(required_packages, rscript)
    
    if install_status['errors'] > 0:
        raise Exception(f"We've encountered some errors while trying to install the following packages: {install_status['with_errors']}")
    
    
def get_rscript_executable(version):
    if version == 'default':
        return 'Rscript'
    else:
        return f"/usr/local/R/{version}/bin/Rscript"
    
    
def find_windows_shared_access(script_loc):
    windows_shared_access = {}
    with open(script_loc) as f:
        lines = f.readlines()
        for i, line in enumerate(lines):
            if '//10.51.2.73' in line:
                print(i, line)
                windows_shared_access[i] = line                
    return windows_shared_access
