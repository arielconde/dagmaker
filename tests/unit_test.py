import sys
import os
AIRFLOW_HOME = os.getenv('AIRFLOW_HOME', '/opt/airflow_home')
sys.path.insert(0, f"{AIRFLOW_HOME}/plugins")

from unittest import TestCase
import subprocess
import time
import datetime

from dagmaker import config
from dagmaker.dagmaker import helpers 

DAGMAKER_PROJECTS_DIR = config.DAGMAKER_PROJECTS_DIR


class DAGMakerTest(TestCase):

    def setUp(self):
        pass

    def test_get_dagmaker_projects(self):
        dagmaker_projects = helpers.get_dagmaker_projects()
        self.assertIsInstance(dagmaker_projects, list)
